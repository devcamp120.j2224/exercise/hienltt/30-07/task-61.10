package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.mocel.Order;
import com.devcamp.pizza365.repository.IOrderRepository;

@RestController
public class OrderController {
    @Autowired
    IOrderRepository iOrderRepository;

    @CrossOrigin
    @GetMapping("/orders")
    public ResponseEntity<List<Order>> getListOrder(){
        try {
            List<Order> lOrders = new ArrayList<>();
            iOrderRepository.findAll().forEach(lOrders :: add);
            return new ResponseEntity<List<Order>>(lOrders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin
    @GetMapping("/order")
    public ResponseEntity<List<Order>> getOrderByCustomerId(@RequestParam(name = "customerId", required = true) long customerId ){
        try {
            List<Order> lOrders = new ArrayList<>();
            for (int i = 0; i < iOrderRepository.findAll().size(); i++){
                if (iOrderRepository.findAll().get(i).getCustomerId() == customerId){
                    lOrders.add(iOrderRepository.findAll().get(i));
                }
            } 
            return new ResponseEntity<List<Order>>(lOrders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

