package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.mocel.Drink;
import com.devcamp.pizza365.repository.IDrinkRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class DrinkController {
    @Autowired
    private IDrinkRepository drinkRepository;

    @GetMapping(value = "/drinks")
    public ResponseEntity<List<Drink>> getAllVouchers() {
        try {
            List<Drink> pDrinks = new ArrayList<Drink>();

            drinkRepository.findAll().forEach(pDrinks::add);

            return new ResponseEntity<>(pDrinks, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
