package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.mocel.Voucher;
import com.devcamp.pizza365.repository.IVoucherRepository;

@RestController
public class VoucherControll {
    @Autowired
    IVoucherRepository iVoucherRepository;

    @CrossOrigin
    @GetMapping("/vouchers")
    public ResponseEntity<List<Voucher>> getListVoucher(){
        try {
            List<Voucher> lVouchers = new ArrayList<>();
            iVoucherRepository.findAll().forEach(lVouchers :: add);
            return new ResponseEntity<List<Voucher>>(lVouchers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
