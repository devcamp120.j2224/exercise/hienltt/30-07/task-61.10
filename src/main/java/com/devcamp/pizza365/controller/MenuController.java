package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.mocel.Menu;
import com.devcamp.pizza365.repository.IMenuRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class MenuController {
    @Autowired
    private IMenuRepository comboMenuRepository;

    @GetMapping(value = "/combomenu")
    public ResponseEntity<List<Menu>> getAllVouchers() {
        try {
            List<Menu> pComboMenu = new ArrayList<Menu>();

            comboMenuRepository.findAll().forEach(pComboMenu::add);

            return new ResponseEntity<>(pComboMenu, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
