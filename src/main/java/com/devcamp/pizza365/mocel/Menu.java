package com.devcamp.pizza365.mocel;

import javax.persistence.*;

@Entity
@Table(name = "combomenu")
public class Menu {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private char size;
    @Column(name = "duong_kinh")
    private int duongKinh;
    @Column(name = "suon_nuong")
    private int suon;
    @Column(name = "salad")
    private int salad;
    @Column(name = "so_luong_nuoc_ngot")
    private int soLuongNuocNgot;
    @Column(name = "don_gia")
    private int donGia;
    
    public Menu() {
    }

    public Menu(char size, int duongKinh, int suon, int salad, int soLuongNuocNgot, int donGia) {
        this.size = size;
        this.duongKinh = duongKinh;
        this.suon = suon;
        this.salad = salad;
        this.soLuongNuocNgot = soLuongNuocNgot;
        this.donGia = donGia;
    }

    public char getSize() {
        return size;
    }

    public void setSize(char size) {
        this.size = size;
    }

    public int getDuongKinh() {
        return duongKinh;
    }

    public void setDuongKinh(int duongKinh) {
        this.duongKinh = duongKinh;
    }

    public int getSuon() {
        return suon;
    }

    public void setSuon(int suon) {
        this.suon = suon;
    }

    public int getSalad() {
        return salad;
    }

    public void setSalad(int salad) {
        this.salad = salad;
    }

    public int getSoLuongNuocNgot() {
        return soLuongNuocNgot;
    }

    public void setSoLuongNuocNgot(int soLuongNuocNgot) {
        this.soLuongNuocNgot = soLuongNuocNgot;
    }

    public int getDonGia() {
        return donGia;
    }

    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }

    
}
