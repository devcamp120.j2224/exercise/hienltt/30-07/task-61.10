package com.devcamp.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.mocel.Drink;

public interface IDrinkRepository extends JpaRepository<Drink, Long> {

}
